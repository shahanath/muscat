<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Ticket_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language', 'function_helper']);
    }

    function add_ticket($data)
    {
        $data = escape_array($data);
        $ticket_data = [
            'ticket_type_id' => $data['ticket_type_id'],
            'user_id' => $data['user_id'],
            'subject' => $data['subject'],
            'email' => $data['email'],
            'description' => $data['description'],
            'status' =>  $data['status'],
        ];

        if (isset($data['edit_ticket']) ) {
        

            $this->db->set($ticket_data)->where('id', $data['edit_ticket'])->update('tickets');
        } else {
           
            $this->db->insert('tickets', $ticket_data);
        }
    }

    function add_ticket_message($data)
    {
        $data = escape_array($data);

        $ticket_msg_data = [
            'user_type' => $data['user_type'],
            'user_id' => $data['user_id'],
            'ticket_id' => $data['ticket_id'],
            'message' => $data['message']
        ];
        if (isset($data['attachments']) && !empty($data['attachments'])) {
            $ticket_msg_data['attachments'] = json_encode($data['attachments']);
        }

        $this->db->insert('ticket_messages', $ticket_msg_data);
    }

    

   
}
