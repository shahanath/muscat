<!-- Izimodal -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/iziModal.min.css' ?>" />
<!-- Favicon -->
<?php $favicon = get_settings('web_favicon');

$path = ($is_rtl == 1) ? 'rtl/' : "";
?>
<link rel="icon" href="<?=base_url($favicon)?>" type="image/gif" sizes="16x16">
<!-- intlTelInput -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/intlTelInput.css' ?>" />
<!-- Bootstrap -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/' . $path . 'bootstrap.min.css' ?>">
<!-- FontAwesome -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/all.min.css' ?>" />
<!-- Swiper css -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/swiper-bundle.min.css' ?>" />
<!-- Bootstrap Tabs -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/bootstrap-tabs-x.min.css' ?>" />
<!-- Sweet Alert -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/sweetalert2.min.css' ?>">
<!-- Select2 -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/select2.min.css' ?>">
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/select2-bootstrap4.min.css' ?>">
<!-- Star rating CSS -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/star-rating.min.css' ?>">
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/theme.css' ?>">
<!-- Custom Stle css -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/' . $path . 'style.css' ?>" />
<!-- Custom Product css -->
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/' . $path . 'products.css' ?>" />
<link rel="stylesheet" href="<?= THEME_ASSETS_URL . 'css/daterangepicker.css' ?>" />
<!-- Color CSS -->
<link rel="stylesheet" href="<?=THEME_ASSETS_URL.'css/colors/peach.css'?>" id="color-switcher">
<!-- Bootstrap -->
<link rel="stylesheet" href="<?=THEME_ASSETS_URL.'css/bootstrap-table.min.css'?>">
<link rel="stylesheet" href="<?=THEME_ASSETS_URL.'css/lightbox.css'?>">
<!-- Jquery -->
<script src="<?= THEME_ASSETS_URL . 'js/jquery.min.js' ?>"></script>
<!-- Date Range Picker -->
<script src="<?= THEME_ASSETS_URL . 'js/moment.min.js' ?>"></script>
<script src="<?= THEME_ASSETS_URL . 'js/daterangepicker.js' ?>"></script>
<!-- Star rating js -->
<script type="text/javascript" src="<?= THEME_ASSETS_URL . 'js/star-rating.js' ?>"></script>
<script type="text/javascript" src="<?= THEME_ASSETS_URL . 'js/theme.min.js' ?>"></script>
<script type="text/javascript">
    base_url = "<?= base_url() ?>";
    currency = "<?= $settings['currency'] ?>";
    csrfName = "<?= $this->security->get_csrf_token_name() ?>";
    csrfHash = "<?= $this->security->get_csrf_hash() ?>";
</script>